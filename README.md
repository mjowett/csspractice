# CSS Practice Exercises #

## Resources: ##

> Using the following demos, or others you may find, then modify and include the
> following exercises into the **index.htm** file (keeping the
> numbering)--***all exs. must use CSS (using your choice of CSS implementation)***:

- [CSS Tutorial](http://qcitr.com/demos/css/ "CSS Tutorial")

- [CSS Primer](http://qcitr.com/courses/tutorials/css/ "CSS Primer")
## Requirements: ##

1. Command to clone this repo!
2. Open **index.htm** and uncomment the lines (one at a time), then test (reload) to see how the ***four*** levels of CSS precedence work.
3. Create a table with a 2 pixel, dotted, navy blue border.
4. Display all h1 headers in cornflower blue.      
5. Display all h1 headers that use emphasis in red.
6. Create a class (myClass) to display a yellow background and brown text. Use the class on one paragraph.
7. Create a class (yourClass) to display a 2 pixel, double border line in green. Use the class on another paragraph.
8. Combine both classes above on a third paragraph.
9. Create an id (myID) to display an aqua background and dark green text. Use the id on one div element.
10. Display all h2 and h3 headers in magenta, bold, with letter spacing of .3 em, a bottom margin of 1.5 em, and in small caps
11. Display an image with a 4 pixel green ridge border.      
12. Create a form with two text fields, and display the text fields with a light blue background and dark blue text.      

   
**Notes:**

Some developer documentation states that @import must come before all other CSS
content, even comments. Though, that particular requirement doesn't appear to be
standardized.

Best Practices (***though, debatable!***): Avoid CSS @import

- [https://www.stevesouders.com/blog/2009/04/09/dont-use-import/](https://www.stevesouders.com/blog/2009/04/09/dont-use-import/ "Using @import")
  
- [https://developer.yahoo.com/performance/rules.html#csslink](https://developer.yahoo.com/performance/rules.html#csslink "Yahoo Rules") 
  
- [https://www.w3docs.com/learn-css/import.html](https://www.w3docs.com/learn-css/import.html "W3 @import documentation") 

**Two reasons to still use @import:**
 
1. You **may** inherit @import style sheets with which you will *have* to work. 
2. You may have a very large number of files that have a linked style sheet.

The easiest way to add additional styles is to use @import, rather than adding links to all of the existing files.

**Recommendation:** "minify" all CSS into one file, to make as few HTTP-requests as possible, 
and to decrease amount of data that needs to be transferred.

**CSS minifying tool:**
http://www.minifycss.com/css-compressor/

And, of course, *everyone* has an opinion...

1. (Update is interesting!): https://stackoverflow.com/questions/10036977/best-way-to-include-css-why-use-import
   
2. https://medium.com/before-semicolon/50-css-best-practices-guidelines-to-write-better-css-c60807e9eee2

3. https://www.webtips.dev/10-best-practices-for-quickly-improving-your-css
